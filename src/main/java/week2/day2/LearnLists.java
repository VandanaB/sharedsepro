package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class LearnLists {

	public static void main(String[] args) {
		List<String> movies = new ArrayList<>();
		movies.add("Bahubali");
		movies.add("Bahubali");
		movies.add("Bahubali");
		movies.add("saamy");
		movies.add("mersal");
		//System.out.println(movies);
		//movies.clear();
		
		//System.out.println(movies.isEmpty());
		int count = 0;
		for (String movie : movies) {
			if(movie.contains("Bahubali")) {
				count++;		
			}	
		}
	
		System.out.println(count);	
	}

}
