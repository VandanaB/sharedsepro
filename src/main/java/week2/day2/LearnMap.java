package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class LearnMap {

	public static void main(String[] args) {
	String a = "vandana";
	
	int count= 1;
	char[] ch = a.toCharArray();
	System.out.println(ch);
	
	//character is key and integer is value
	Map<Character, Integer> occ1 = new HashMap<>();
	
	for (char c : ch) {
		if(occ1.containsKey(c)) {    // 
			count=count+1;
			
		}else count=1;
		occ1.put(c,count);	
	}
        System.out.println(occ1);
	}

}
