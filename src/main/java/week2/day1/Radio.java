package week2.day1;

// abstract class uses key word "abstract"
// has both normal method and abstract method

public abstract class Radio {
	// Normal method - implementable
	public void playStation() {
		System.out.println("play");
		
	}
	//abstract method
	public abstract void changeBattery(); 
		
	

}
