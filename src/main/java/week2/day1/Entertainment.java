package week2.day1;

public interface Entertainment {
	public void maxSize();
	public void maxBrightness();
	public void maxVolume();

}
