package week2.day1;

public class SmartTV extends Television {
	public void internetConnect() {
		System.out.println("wifi option available");
	}
	public void progRecording() {
		System.out.println("Record current program");
	}
	public void externalDevices() {
		System.out.println("devices connected");
	}

}
