package week2.day1;

// this is a subclass that extends abstract class radio
// hence object can be created
public class PocketRadio extends Radio {
	
	public static void main(String[] args) {
		
		PocketRadio r = new PocketRadio();
		r.changeBattery();
		r.playStation();
			
	}
	@Override
	//abstract method from abstract class
	public void changeBattery() {
	System.out.println("change battery");
		
	}
	

	
	

}
