package week2.day1;

public class mainMethodTV  {

	public static void main(String[] args) {
		// only Interface methods are called
		//Entertainment is the super class
		Entertainment tv = new Television();
		tv.maxSize();
		
		// Television methods and Interface are called
		// Television Superclass implements Interface
		Television tv1 = new Television();
        tv1.channelSwap();
        tv1.maxSize();
        
        // SmartTV Television Interface all methods are called
        //SmartTV extends Television Class which implements Entertainment Interface
        SmartTV tv2 = new SmartTV();
        tv2.progRecording();
        tv2.channelSwap();
        tv2.maxSize();
	}

}
