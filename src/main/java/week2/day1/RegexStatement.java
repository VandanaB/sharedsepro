package week2.day1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexStatement {

	public static void main(String[] args) {
		String txt = "vandana.balraj@gmail.com";
		String pattern = "(.+)+@(.+)";
		Pattern c = Pattern.compile(pattern);
		Matcher m = c.matcher(txt);
		System.out.println(m.matches());
		System.out.println(m.group());

	}

}
