package week1.day1;

import java.util.Scanner;

public class SwitchCase {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")// 
		
	//to get input from user we have imported the class and called Scanner class here
		
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a");
		int a = scan.nextInt();
		System.out.println("enter b");
		int b = scan.nextInt();
		System.out.println("enter opertion");
		String operation = scan.next();
		
		switch (operation) {
		case "add":System.out.println("Add a and b " + (a+b));
			break;
		case "sub":System.out.println("Sub a and b " + (a-b));
			break;
		default:System.out.println("invalid");
			break;
		}
		

	}

}
