package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;



public class ProjectMethods extends SeMethods {
	
	@BeforeSuite//(groups= {"common"})
	public void beforeSuite() {
		startResult();
	}
	
	@Parameters({"url","username","password"})
	
	@BeforeMethod//(groups= {"common"})
	
	public void login(String url, String username, String password) {
		
		testCaseLevel();

		startApp("chrome", url);
	   WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrm = locateElement("LinkText", "CRM/SFA");
		click(eleCrm);
		
	}
	
	@AfterMethod//(groups= {"common"})
	public void closeApp() {
		closeBrowser();
	}
	
	@AfterSuite//(groups= {"common"})
	public void endTestCase() {
		endResult();
	}
	
}
