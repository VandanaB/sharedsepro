package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assign {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("RRRajuUUU_1984");
		driver.findElementByLinkText("Check Availability").click();
		WebElement p = driver.findElementById("userRegistrationForm:useravailn");
		String print = p.getText();
		System.out.println(print);
		
		driver.findElementById("userRegistrationForm:password").sendKeys("ghdykek37477");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("ghdykek37477");
		WebElement securityDrop = driver.findElementById("userRegistrationForm:securityQ");
		Select drop = new Select(securityDrop);
		drop.selectByVisibleText("What is your pet name?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("rony");
		WebElement language = driver.findElementById("userRegistrationForm:prelan");
		Select drop2 = new Select(language);
		drop2.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:firstName").sendKeys("huhsgdubeu");
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		WebElement dates = driver.findElementById("userRegistrationForm:dobDay");
		Select day = new Select(dates);
		day.selectByVisibleText("05");
		WebElement months = driver.findElementById("userRegistrationForm:dobMonth");
		Select month = new Select(months);
		month.selectByIndex(3);
		WebElement years = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select year = new Select(years);
		year.selectByValue("1989");
		
		WebElement occp = driver.findElementById("userRegistrationForm:occupation");
		Select occ = new Select(occp);
		occ.selectByValue("3");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select con = new Select(country);
		con.selectByValue("94");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600117",Keys.TAB);
		Thread.sleep(2000);
		
		WebElement input = driver.findElementById("userRegistrationForm:cityName");
		Select ip = new Select(input);
		ip.selectByValue("Kanchipuram");
		
	
	}

}
