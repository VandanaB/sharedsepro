package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LinkAvailable {

	public static void main(String[] args) {
		/*invoke browser
		 for path if multiple browsers// (getting .exe file)
		ChromeDriver class for driver*/
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	
	//for maximize the window
	driver.manage().window().maximize();
	
	// for loading the url
	driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	List<WebElement> tt = driver.findElementsByTagName("a");
	System.out.println(tt.size());
	for (WebElement eachwebElement : tt) {
		System.out.println(eachwebElement.getText());
		
	}
	WebElement fourthOne = tt.get(2);
	fourthOne.click();
	String text = fourthOne.getText();
	System.out.println(text);
	
	

	}

}
