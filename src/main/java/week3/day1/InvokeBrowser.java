package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class InvokeBrowser {

	public static void main(String[] args) {
		
		/*invoke browser
		 for path if multiple browsers// (getting .exe file)
		ChromeDriver class for driver*/
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	
	//for maximize the window
	driver.manage().window().maximize();
	
	// for loading the url
	driver.get("http://leaftaps.com/opentaps/");
	
	//inspect element on the web page and find the locator enter the values
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	
	//locator is class
	driver.findElementByClassName("decorativeSubmit").click();
	
	//locator is link. give preference to text
	driver.findElementByPartialLinkText("CRM/SFA").click();
	driver.findElementByPartialLinkText("Create Lead").click();
	
    driver.findElementById("createLeadForm_companyName").sendKeys("TCS");
    driver.findElementById("createLeadForm_firstName").sendKeys("abc");
    driver.findElementById("createLeadForm_lastName").sendKeys("xyz");
    
    
    
    
    WebElement source  = driver.findElementById("createLeadForm_dataSourceId");
    Select drop = new Select(source);
    drop.selectByVisibleText("Public Relations");
    WebElement source2 = driver.findElementById("createLeadForm_marketingCampaignId");
    Select drop2 = new Select(source2);
    drop2.selectByValue("CATRQ_AUTOMOBILE");
    WebElement source3 = driver.findElementById("createLeadForm_industryEnumId");
    Select drop3 = new Select(source3);
    List<WebElement> alloptions = drop3.getOptions();
    for (WebElement eachOption : alloptions) {
    	//System.out.println(eachOption.getText());
    	String txt = eachOption.getText();
    	
       if( txt.startsWith("M"))
        System.out.println(txt);
		
	}
    
  
   
    
    
   // driver.findElementByClassName("smallSubmit").click();
    
	}

}
