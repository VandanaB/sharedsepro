package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) throws InterruptedException {
		/*invoke browser
		 for path if multiple browsers// (getting .exe file)
		ChromeDriver class for driver*/
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	
	//for maximize the window
	driver.manage().window().maximize();
	driver.get("https://erail.in");
	
	driver.findElementById("txtStationFrom").clear();
    driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
    driver.findElementById("txtStationTo").clear();
    driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
    WebElement ele = driver.findElementById("chkSelectDateOnly");
    if(ele.isSelected()) {
    	ele.click();
    }
    Thread.sleep(3000);
    
    WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
    List<WebElement> allRows = table.findElements(By.tagName("tr"));
    System.out.println(allRows.size());
    for (WebElement eachRow : allRows) {
		List<WebElement> allColn = eachRow.findElements(By.tagName("td"));
		String text = allColn.get(1).getText();
		System.out.println(text);
			
	}
    
    String row3 = allRows.get(2).getText();
	System.out.println(row3);
    
    
   
    
    
    
    
    
    
    
    
   
    
  
	}

}