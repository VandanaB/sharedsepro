package week3.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class XpathAssign {

	public static void main(String[] args) {
		
		/*invoke browser
		 for path if multiple browsers// (getting .exe file)
		ChromeDriver class for driver*/
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	
	//for maximize the window
	driver.manage().window().maximize();
	
	// for loading the url
	driver.get("http://leaftaps.com/opentaps/");
	
	//inspect element on the web page and find the locator enter the values
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	
	//locator is class
	driver.findElementByClassName("decorativeSubmit").click();
	
	//locator is link. give preference to text
	driver.findElementByPartialLinkText("CRM/SFA").click();
	driver.findElementByPartialLinkText("Create Lead").click();
	driver.findElementByXPath("//input[@id='createLeadForm_companyName']").sendKeys("TCS");
	driver.findElementByXPath("//select[@id='createLeadForm_dataSourceId']/option[4]").click();
	driver.findElementByXPath("//input[@id='createLeadForm_parentPartyId']/following-sibling::a/img").click();
	driver.findElementByLinkText("Company").click();	

	}

}
