package testcases;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
import week6.day2.ReadExcel;

public class TC003FindLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "Findlead";
		testDesc = "Find a lead";
		author = "vandana";
		category = "smoke";
	}
	@Test(dataProvider="data",enabled=false)
	
	public void findLead(String comName, String fName, String lName, String email, String cNum) {
	
		 //find lead page
		click(locateElement("LinkText", "Leads"));
	    WebElement findLeadsTab = locateElement("LinkText", "Find Leads");
	    click(findLeadsTab);
	   type(locateElement("xpath", "(//div[@tabindex='-1'])[19]//input"), fName); 
		type(locateElement("xpath", "(//div[@tabindex='-1'])[20]//input"), lName);
		type(locateElement("xpath", "(//div[@tabindex='-1'])[21]//input"), comName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
        click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
        
        }
	
	@DataProvider(name="data",indices= {0})
	public Object[][] fetchData() throws IOException{
		Object[][] data = ReadExcel.readXcel();
		return data;
		
	}
	
      }