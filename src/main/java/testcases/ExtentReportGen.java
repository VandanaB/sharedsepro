package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportGen {
	@Test
	public void Reports() throws IOException {
		//Non writable mode html report
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		//to take history if updated
		html.setAppendExisting(true);
		
		//make to writable mode
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		
		ExtentTest logger = extent.createTest("TC002_CreateLead", "Lead is Created");
		logger.assignAuthor("Vandana");
		logger.assignCategory("SMOKE");
		logger.log(Status.PASS, "The DemoSalesmanager Entered Successfully"
				,MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		
		logger.log(Status.PASS, "The Data  crmsfa Enetered Successfully"
				,MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build() );
		
		logger.log(Status.PASS, "The CreateLead Button is Clicked"
				,MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
		
		extent.flush();
		
		
	}
	

}
