package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import week6.day2.ReadExcel;
 
public class TC005_Edit extends ProjectMethods{
	
	@BeforeTest(groups= {"smoke"})
	
	public void setData1() {
		testCaseName = "TC007";
		testDesc = "Edit A Lead";
		author = "gopi";
		category = "smoke";
	}

    // @Test(invocationCount=2,invocationTimeOut=30000,groups= {"smoke"})
     @Test(dataProvider="data")
     
     public void edit(String comName,String fName, String lName,String email,String num) {
    	 
    	 click(locateElement("LinkText", "Leads"));
 	     click(locateElement("LinkText", "Find Leads"));
  	     type(locateElement("xpath", "(//div[@tabindex='-1'])[19]//input"), fName); 
  		 type(locateElement("xpath", "(//div[@tabindex='-1'])[20]//input"), lName);
  		 type(locateElement("xpath", "(//div[@tabindex='-1'])[21]//input"), comName);
  		 click(locateElement("xpath", "//button[text()='Find Leads']"));
    	 click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
    	 click(locateElement("LinkText", "Edit"));
    	 clearText(locateElement("updateLeadForm_companyName"));
    	 type(locateElement("updateLeadForm_companyName"),comName );
    	 WebElement dd = locateElement("updateLeadForm_industryEnumId");
    	 selectDropDownUsingIndex(dd, 9);
    	 click(locateElement("name", "submitButton"));	
     }
     
     @DataProvider(name="data", indices= {1})
     public Object[][] fetchData() throws IOException{
    	 Object[][] data = ReadExcel.readXcel();
		return data;
     }
     
     
     
     
     
     
}
