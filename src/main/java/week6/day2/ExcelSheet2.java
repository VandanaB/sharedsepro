package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelSheet2 {
	
	public static Object[][] xCel() throws IOException {
		@SuppressWarnings("resource")
		XSSFWorkbook wb = new XSSFWorkbook("./data/Book2.xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		int cellCount = sheet.getRow(0).getLastCellNum();
		
		Object[][] data = new Object[rowCount][cellCount];
		
		 for(int i=1;i<=rowCount;i++) {
			XSSFRow row = sheet.getRow(i);
			
			try {
				for(int j=0;j<cellCount;j++) {
					XSSFCell cell = row.getCell(j);
					String value = cell.getStringCellValue();
					System.out.println(value);
					data[i-1][j]=value;
				}
			} catch (NullPointerException e) {
			System.out.println("");
			}
		}
		wb.close();
		return data;
	}

	
	
}
