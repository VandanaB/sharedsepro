package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertLearn {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//p[@id='demo']/preceding-sibling::button").click();
		driver.switchTo().alert().sendKeys("Vandana");
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		driver.findElementById("tryhome").click();;
		Set<String> allWindows = driver.getWindowHandles();
		List<String> eachWindow = new ArrayList<>();
		eachWindow.addAll(allWindows);
		driver.switchTo().window(eachWindow.get(1));
		driver.findElementByXPath("(//a[text()='Try it Yourself �'])[1]").click();
		driver.quit();
		
		
	}

}
