package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowLearn {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// for loading the url
		driver.get("http://leaftaps.com/opentaps/");
		
		//inspect element on the web page and find the locator enter the values
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		
		//locator is class
		driver.findElementByClassName("decorativeSubmit").click();
		
		//locator is link. give preference to text
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByPartialLinkText("Create Lead").click();
		
		
		driver.findElementByPartialLinkText("Merge Leads").click();
		driver.findElementByXPath("//table[contains(@id,'widget')]/following-sibling::a/img").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> eachWindow = new ArrayList<>();
		eachWindow.addAll(allWindows);
		driver.switchTo().window(eachWindow.get(1));
		
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Vandana");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		driver.findElementByXPath("//a[text()='Vandana']").click();
		driver.switchTo().window(eachWindow.get(0));
		driver.findElementByXPath("//table[@name='ComboBox_partyIdTo']/following-sibling::a/img").click();
		Set<String> allWindows1 = driver.getWindowHandles();
		List<String> eachWindow1 = new ArrayList<>();
		eachWindow1.addAll(allWindows1);
		driver.switchTo().window(eachWindow1.get(1));
		driver.findElementByXPath("//input[@name='firstName']").sendKeys("Seetha");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		driver.findElementByXPath("//a[text()='Seetha']").click();
		driver.switchTo().window(eachWindow1.get(0));
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}

}
