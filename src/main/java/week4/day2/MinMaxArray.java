package week4.day2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MinMaxArray {

	public static void main(String[] args) {
	
	List<Integer> num = new ArrayList<>();
	num.addAll(Arrays.asList( new Integer[] {11,13,3,5,7,18,8,9}));

	System.out.println(Collections.max(num));
	System.out.println(Collections.min(num));
	

	}

}

