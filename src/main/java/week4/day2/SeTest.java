package week4.day2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeTest {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/?affid=digi7medi&affExtParam1=KAZAS");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//for unidentified buttons use keyboard keys
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		driver.findElementByName("q").sendKeys("IPhoneX");
		driver.getKeyboard().sendKeys(Keys.ENTER);
		
		//taking a list of data nd assigning toa list
	    List<WebElement> allPrice = driver.findElementsByXPath("//div[contains(@class,'_1vC4OE ')]");
	   // System.out.println(allPrice.size()); 
	    //taking each element from the list
	    List<String> pricelst = new ArrayList<>();
	    for (WebElement eachPrice : allPrice) {
	    	pricelst.add(eachPrice.getText());	//dont forget getText()
		}
	  System.out.println(pricelst);
	  
	  //list above each element nd remove non digit char using Regex
	  List<String> pricePtrn = new ArrayList<>();
	  for (String eachpricePtrn : pricelst) {
		  pricePtrn.add(eachpricePtrn.replaceAll("[^0-9]", ""));
	}
	  System.out.println(pricePtrn);
	  
	  //remove duplicates by using list in set
	  Set<String> newPricelst = new TreeSet<>();
	  newPricelst.addAll(pricePtrn);  
	  System.out.println(newPricelst);
	  
	 //finding max number from list
      int frstMax=0;
	  for (String eachnpl : newPricelst) {
		int max = Integer.parseInt(eachnpl);  //convert String to Integer
		if(max>frstMax) {
			frstMax=max;
		}//System.out.println(frstMax);
	 }   //finding Second max value
		  int secMax=0;
			 for (String eachnplst : newPricelst) {
				int maxi = Integer.parseInt(eachnplst);
		if((maxi>secMax)&&(maxi!=frstMax)) {
			secMax=maxi;
		}
	}
			 System.out.println(secMax);
	 
	     //map the value of mobile name and price 
	   List<WebElement> mobNames = driver.findElementsByXPath("//div[@class='_3wU53n']");
	 
	 
	   Map<WebElement,String> mobName = new LinkedHashMap<>();
	  for(int i=1;(i<mobNames.size()&&i<pricePtrn.size());i++) {
		mobName.put(mobNames.get(i), pricePtrn.get(i));
	}
	
	 for (Entry<WebElement,String>mobname : mobName.entrySet()) {
		 if(mobname.getValue().contains(Integer.toString(secMax))) {
			 System.out.println(mobname.getKey().getText());
			 mobname.getKey().click();
		 }
		
	}
	 Set<String> wnds = driver.getWindowHandles();
	 List<String> wnd = new ArrayList<>();
	  wnd.addAll(wnds);
	  driver.switchTo().window(wnd.get(1));
	   
	   
	 
	
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	    
	   
	
	    
	    
		
			
			
			
		}
	    
		

	}


