package week5.day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class ZoomCar extends ProjectMethods {
	
	@BeforeTest		
	
	public void setData() {
		testCaseName ="Launch Browser";
		testDesc = "Browser is Launched Successfully";
		category = "Smoke";
		author = "Vandana";
	}
	@Test	
	public void launch() throws InterruptedException {
		
	WebElement startFeild = locateElement("LinkText", "Start your wonderful journey");	
	click(startFeild);	
	WebElement pp = locateElement("xpath", "//div[@class='heading']/following-sibling::div[1]");
	click(pp);
	WebElement nxtBtn = locateElement("class", "proceed");
	click(nxtBtn);

 // Get the current date
 		Date date = new Date();
 // Get only the date (and not month, year, time etc)
 		
 		DateFormat sdf = new SimpleDateFormat("dd"); 
 // Get today's date
 		String today = sdf.format(date);
 // Convert to integer and add 1 to it
 		Integer tomorrow = Integer.parseInt(today)+1;
 // Print tomorrow's date
 		System.out.println(tomorrow);
 		
 		WebElement tomo = locateElement("xpath","//div[contains(text(),'"+tomorrow+"')]");
 		click(tomo);
 		
 	
  	   WebElement nxtBtn1 = locateElement("class", "proceed");
 		click(nxtBtn1);
 		
 		Thread.sleep(2000);
 		WebElement currDte= locateElement("xpath","(//div[contains(@class,'day picked ')])");
 		
 	    verifyPartialText(currDte, tomorrow.toString());
 		
 	   WebElement nxtBtn2 = locateElement("class", "proceed");
		click(nxtBtn2);
		List<WebElement> lstPrc = driver.findElementsByXPath("//div[@class='price']");
		List<String> eachPrcs = new ArrayList<>();
		for (WebElement eachPrc : lstPrc) {
			eachPrcs.add(eachPrc.getText());
			
		}//System.out.println(eachPrcs);
 		
	List<String> intPrc = new ArrayList<>();
	for (String prc : eachPrcs) {
		intPrc.add(prc.replaceAll("[^0-9]", ""));
		
	}System.out.println(intPrc);
	
	Set <String> newIntLst = new LinkedHashSet<>();
	newIntLst.addAll(intPrc);
	System.out.println(newIntLst);
	
	 int firstMax =0;
	 for (String eachPrice : newIntLst) {
		 int parseInt = Integer.parseInt(eachPrice);
		 if(parseInt>firstMax) {
			 firstMax=parseInt;	 
		 }
		
	}System.out.println(firstMax);

	
	String pricePath = "//div[contains(text(),'"+firstMax+"')]";
	System.out.println(pricePath);
	WebElement carName = locateElement("xpath", pricePath+"/preceding::h3");
	
	
	String maxvalueCarName = carName.getText();
	System.out.println(maxvalueCarName);
	driver.getKeyboard().sendKeys(Keys.ESCAPE);
	
	
	WebElement bookButton = locateElement("xpath", pricePath+"/following-sibling::button");
	click(bookButton);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	}

}
